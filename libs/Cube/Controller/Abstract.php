<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
abstract class Cube_Controller_Abstract 
{
	protected $_request = NULL;
	protected $_session = NULL;
	protected $_config = NULL;
	protected $_front = NULL;	
	protected $view = null;
		
	public function __construct($request, $front)
	{
		$this->_request = $request;
		$this->_session = Cube_Registry::get('session');
		$this->_config = Cube_Registry::get('config');
		$this->_front = $front;
		$this->initView();
		$this->init();
	}

	protected function _forward($controller, $action, $module = null, $params = array())
	{
		$this->_request->setDispatched(FALSE);
		$this->_request->setController($controller);
		$this->_request->setAction($action);
		$this->_request->setParams($params);
		if (!is_null($module)) {
			$this->_request->setModule($module);
		}
	}
	
	public function run($action)
	{
		if (!method_exists($this, $action)) {
			$action = NULL;
			$action = $this -> _defaultAction();
			if (!is_string($action) || $action == NULL) {
				throw new Cube_Exception('Akcja nie istnieje');
				return;
			}
		}
		$this->$action();
		$this->_sendOutput();
	}
	
	protected function _redirect($controller, $action, $module = null)
	{
		if (!is_null($module)) $module = $module.',';
		$url = $module."$controller,$action.html";
		header('Location: '.$url);
	}
	
	protected function _defaultAction()
	{
		return $this->_front->getDefaultAction().'Action';
	}
	
	protected function _request()
	{
		return $this->_request;
	}
	
	public function init() {}
	
	protected function initView() 
	{
		Cube_Loader::loadClass('Cube_View');
		$this->view = new Cube_View($this->_request);
	}	
	
	protected function _sendOutput()
	{
		$this->view->output();
	}
}

?>
