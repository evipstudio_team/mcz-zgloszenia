<?php

/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 

class Cube_Pages_Advanced 
{
	private $_start; // numer pierwszego elementu do wyświetlenia
    private $_elements; // całkowita ilość elemntów
    private $_perPage; // ilośc elemntów na stronę
    private $_template; // :value =  wartość, :id = id 
    private $_currTemplate; // :id = id
    private $_result; 
    private $_currentPageId;
    private $_numberOfPages;

    public function __construct($start, $elements, $perPage, $template, $currTemplate = ' :id ') 
    {
    	$this->_start = $start;	
		$this->_elements = $elements;
		$this->_perPage = $perPage;
		$this->_template = $template;
		$this->_currTemplate = $currTemplate;
		
		$numberOfPages = ceil($elements/$perPage); 
		$currentPageId = floor($start/$perPage) + 1; 
		
		$this->_numberOfPages = $numberOfPages;
		$this->_currentPageId = $currentPageId;
		
		$result = '';
		
		if ($numberOfPages > 10) {
				$isFirstSegment = ($currentPageId < 6) ? true : false;
				$isLastSegment = ($currentPageId > ($numberOfPages - 6)) ? true : false; 			
				$showCenterSegment = (!$isFirstSegment && !$isLastSegment) ? true : false;
				
				if ($showCenterSegment) {
					$result .= $this->_profile(0, 1) 
					         . $this->_profile(1*$perPage, 2) 
							 . $this->_profile(2*$perPage, 3).' ...';
					$result .= $this->_profile((($currentPageId-2)*$perPage), ($currentPageId-1)) 
							 . $this->_profile(-1, $currentPageId) 
							 . $this->_profile((($currentPageId)*$perPage), $currentPageId+1).' ... ';
					$result .= $this->_profile((($numberOfPages-3)*$perPage), ($numberOfPages-2)) 
					         . $this->_profile((($numberOfPages-2)*$perPage), ($numberOfPages-1)) 
							 . $this->_profile((($numberOfPages-1)*$perPage), $numberOfPages);
				} else {
					if ($isFirstSegment) {
						for ($i = 1; $i < 7; $i++) 
						{
							$startValue = ($i-1)*$perPage; 
							if ($i == $currentPageId) $result .= $this->_profile(-1, $i); 
							else $result .= $this->_profile($startValue, $i); 
						}
						$result .= ' ... ' . $this->_profile((($numberOfPages-3)*$perPage), ($numberOfPages-2)) 
					         	 . $this->_profile((($numberOfPages-2)*$perPage), ($numberOfPages-1)) 
							 	 . $this->_profile((($numberOfPages-1)*$perPage), $numberOfPages);					
					}
					elseif ($isLastSegment) {
						$result .= $this->_profile(0, 1) 
					             . $this->_profile(1*$perPage, 2) 
							     . $this->_profile(2*$perPage, 3).' ...';
						
						for ($i = $numberOfPages-6; $i <= $numberOfPages; $i++) 
						{
							$startValue = ($i-1)*$perPage; 
							if ($i == $currentPageId) $result .= $this->_profile(-1, $i); 
							else $result .= $this->_profile($startValue, $i);  
						}						
										
					} else { 
						throw new exception('ani firstSegment ani LastSegment ani show center'); 
					}
				}		
		} else {
			for ($i = 0; $i < $numberOfPages; $i++) 
			{
				$a = $i + 1;
				$startValue = $i*$perPage; 
				if ($a == $currentPageId) $result .= $this->_profile(-1, $a); 
				else $result .= $this->_profile($startValue, $a); 
			}
		}
		
		$this->_result = $result;	
	}
	
	private function _profile($value, $id)
	{
		if ($value < 0) {
			$return = str_replace(':id', $id, $this->_currTemplate);
		} else {
			$return = str_replace(':value', $value, $this->_template);
			
			if ($id == $this->_currentPageId) $return = str_replace(':id', '<span>'.$id.'</span>', $return);	
			else $return = str_replace(':id', $id, $return);	
		}	
		return $return; 
	}
	
	public function getPages()
	{
		return $this->_result;
	}
	
	public function getCurrent()
	{
		return $this->_currentPageId;
	}
	
	public function getNext()
	{
		return ($this->_currentPageId + 1);
	}
	
	public function getPrev()
	{
		return ($this->_currentPageId - 1);
	}

	public function getNextLink($template)
	{
		return str_replace(':value', ((($this->_currentPageId-1)*$this->_perPage)+$this->_perPage), $template);
	}
	
	public function getPrevLink($template)
	{
		return str_replace(':value', ((($this->_currentPageId-1)*$this->_perPage)-$this->_perPage), $template);
	}
	
	public function hasNext()
	{
		if (($this->_currentPageId + 1) > $this->_numberOfPages) return false;
		return true;
	}
	
	public function hasPrev()
	{
		if (($this->_currentPageId - 1) > 0) return true;
		return false;
	}		
}

?>
