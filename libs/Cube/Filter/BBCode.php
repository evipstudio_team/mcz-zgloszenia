<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_BBCode implements Cube_Filter_Interface
{
    public function __construct($params)
    {	
    }

    public function filter($value)
    {	
    	$value = strip_tags($value);
    	$value = htmlentities($value, ENT_QUOTES, 'UTF-8');
		$value = nl2br($value);
		$value = str_replace('[b]', '<strong>', $value);
		$value = str_replace('[/b]', '</strong>', $value);
		$value = str_replace('[u]', '<span class="u">', $value);
		$value = str_replace('[/u]', '</span>', $value);
		$value = str_replace('[i]', '<span class="i">', $value);
		$value = str_replace('[/i]', '</span>', $value);
		$value = addslashes($value);
		return $value;
    }
}
