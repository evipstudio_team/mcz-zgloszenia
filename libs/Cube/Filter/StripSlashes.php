<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_StripSlashes implements Cube_Filter_Interface
{

    public function __construct($params)
    {
    }

    public function filter($value)
    {
        return stripslashes($value);
    }
}
