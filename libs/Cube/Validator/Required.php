<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */
require_once 'Cube/Validator/Interface.php';

class Cube_Validator_Required implements Cube_Validator_Interface
{
    public function __construct($params) {}
    public function validate($value)
    {
    	$value = trim($value);
		if (empty($value)) return false;
		return true;
    }
}
