<?php 

class AuthController extends Cube_Controller_Abstract
{
	public function indexAction()
	{
		//$this->view->setTemplate('adminLogin');
		$this->view->noRender();
	}
		
	public function loginAction()
	{
		$this->view->noRender();
		$this->view->noOutput();	
		if ($this->_request->isPost()) {
			$session = Cube_Registry::get('session');
			$username = clear($_POST['username']);
			$password = clear($_POST['password']);
			$r = $session->login($username, $password);
			$this->_redirect('index', 'index');			
		} 
	}
	
	public function logoutAction()
	{
		$this->view->noRender();
		$this->view->noOutput();
		$this->view->setTemplate('default');
		$this->_session->logout();
		$this->_redirect('index', 'index');	
	}
	
	public function setlanguageAction()
	{
		$lang = $this->_request()->getParam('lang', $this->_session->getDefaultLanguage());
		$this->_session->setLanguage($lang);
		header('Location: index.html');
	}

	public function setthemeAction()
	{
		$theme = $this->_request()->getParam('theme', $this->_session->getDefaultTheme());
		$this->_session->setTheme($theme);
		header('Location: index.html');
	}			
}

?>
