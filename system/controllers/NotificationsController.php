<?php
define('CSV_DIR', 'public/csv/');
define('STRING_SEPARATOR', '^'); //separator uzyty do stringu search

class NotificationsController extends Cube_Controller_Abstract
{
	private $_pp; // photos per page
	//private $_from='krzysztof@evipstudio.pl';
	private $_from;
	private $_id = null;
	private $_column=null;
	private $_state=null;
	private $_search=null;
	private $_start_date=null;
	private $_end_date=null;


	private function paging($rows)
	{

		//stronicowanie
		$this->_pp=20;
		$rows=$this->view->rows;
		$this->view->start = $start = $this->_request()->getParam('start', 0);
		$search_string=$this->_search.STRING_SEPARATOR.$this->_start_date.STRING_SEPARATOR.$this->_end_date;
		//echo 'start='.$this->view->start;
		//link do cyferek
		$template = ' <a href="zgloszenia,sort,column_'.$this->_column.',state_'.$this->_state.',search_'.$search_string.',start_:value.html">:id</a> ';
		//link do nastepny, poprzedni
		$this->view->template = 'zgloszenia,sort,column_'.$this->_column.',state_'.$this->_state.',search_'.$search_string.',start_:value.html';

		$rows_amount = sizeof($rows);
		// PAGES
		Cube_Loader::loadClass('Cube_Pages_Advanced');
		$pages = new Cube_Pages_Advanced($start, $rows_amount, $this->_pp, $template);

		$tmp = array();
		foreach ($rows as $k => $v)
		{
			if ( ($k + 1) > $start && ($k + 1) <= ($start + $this->_pp) )
				$tmp[] = $v;
		}

		$this->view->pages = $pages;	//obiekt ze stronami
		// $this->view->products = $tmp;
		$this->view->rows_pages = $tmp;  //wyniki na stronie
	}


	private function create_where($search,$start_date,$end_date)
	{
		$where_id='n.id like "%'.$search.'%"';
		$where_title='n.subject like "%'.$search.'%"';
		$where_status='ns.title like "%'.$search.'%"';
		$where_type='nt.title like "%'.$search.'%"';
		$where_priority='np.title like "%'.$search.'%"';
		$where_priority='np.title like "%'.$search.'%"';

		$where_unit='nu.title like "%'.$search.'%"';
		$where='('.$where_id.' OR '.$where_title.' OR '.$where_status.' OR '.$where_type.' OR '.$where_priority.' OR '.$where_unit.')';

		$where_date=NULL;
		if (($start_date != '' and is_numeric($start_date)) and ($end_date != '' and is_numeric($end_date)))
			//jezeli jest start_date i end_date
			$where_date=' AND (add_date BETWEEN '.$start_date.' AND '.$end_date.')';
		if (($start_date != '' and is_numeric($start_date)) and ($end_date == '' or !is_numeric($end_date)))
			//jezeli jest tylko start_date
			$where_date=' AND add_date > '.$start_date;
		if (($start_date == '' or !is_numeric($start_date)) and ($end_date != '' and is_numeric($end_date)))
			//jezeli jest tylko end_date
			$where_date=' AND add_date < '.$end_date;

		//echo $where.$where_date;
		return $where.$where_date;
	}

	//SORTOWANIE

	private function sort($field,$sort)
	//public function sortLoginAction()
	{

$model = new Notifications();
		if ($sort==1)
			$order='ASC';
		else if ($sort==2)
			$order='DESC';


		$start_date=$this->_start_date;
		$end_date=$this->_end_date;
		$this->check_date($start_date,$end_date);
		$where=$this->create_where($this->_search,$start_date,$end_date);


		$id_user=$this->_session->getUserId();
		$this->view->rows = $model->getNotifications('n.id_user='.$id_user.' AND '.$where,''.$field.' '.$order.'');
		//sort($order, $field);
		$this->view->render('index');

	}

private function sortDate()
	{
		$field='n.add_date';
		$sort=$this->_state;
		$this->sort($field,$sort);
	}
	private function sortTitle()
	{
		$field='n.subject';
		$sort=$this->_state;
		$this->sort($field,$sort);
	}

	private function sortStatus()
	{
		$field='ns.title';
		$sort=$this->_state;
		//echo "SORT=$sort";
		$this->sort($field,$sort);
	}

	private function sortType()
	{
		$field='nt.title';
		$sort=$this->_state;
		//echo "SORT=$sort";
		$this->sort($field,$sort);
	}

	private function sortPriority()
	{
		$field='np.title';
		$sort=$this->_state;
		//echo "SORT=$sort";
		$this->sort($field,$sort);
	}

	private function sortUnit()
	{
		$field='nu.title';
		$sort=$this->_state;
		//echo "SORT=$sort";
		$this->sort($field,$sort);
	}

	private function sortID()
	{
		$field='n.id';
		$sort=$this->_state;
		$this->sort($field,$sort);
	}

	private function sort_prepare()
	{
		//przygotuj dane do sortowania.
		//Wykorzystywane dla PDF i wydruku.

		if( ( $this->_column =='' || !is_numeric($this->_column)) ||
		    ( $this->_state ==''  || !is_numeric($this->_state )) )
		{
			$this->_column=0;
			$this->_state=2;
		}
		else
		{
			//zamien statusy, potrzebne do poprawnego sortowania danych dla pdf i wydruku
			if($this->_state==1)
				$this->_state=2;
			else if($this->_state==2)
				$this->_state=1;
		}
	}

	//dodaj do notifications miasto z jakim zwiazane jest zgloszenie
	private function _get_city_from_unit($notifications)
	{
		$model= new NotificationsUnit();
		$rows=$model->getUnits('u.id='.$notifications['id_unit'],null,'*,c.title as city');
		//print("Wynik rows:<BR><pre>");
		//print_r($rows);
		//print("<BR><pre>") ;
		return $rows[0]['city'];
	}


	private function send_mail_add($id)
	{
//die('wejscie');
		$from=$this->_from;

		//pobierz adresy emial wszystkich administratorow
		$model = new Admins();
		$email=$model->getAllAdminsEmail();
		//print("Wynik notification:<BR><pre>");
		//print_r($email);
		//print("<BR><pre>") ;
		//extit;
		$model = new NotificationsHistory();
		//echo 'ID='.$id;
		$data=$model->get($id);

		//pobierz informacje o zgloszeniu zwiazanych z danym messagem
		$model= new Notifications();
		$rows=$model->getNotifications('n.id='.$data['id_notification']);
		//print_r($rows);
		$notifications=$rows[0];

		//print("Wynik notification:<BR><pre>");
		//print_r($notifications);
		//print("<BR><pre>") ;

		$notifications['city']=$this->_get_city_from_unit($notifications);
		//echo 'CITY='.$notifications['city'];
		//print("Wynik email:<BR><pre>");
		//print_r($email);
		//print("<BR><pre>") ;

		//print("Wynik data:<BR><pre>");
		//print_r($data);
		//print("<BR><pre>") ;

		return ($this->send_mail($from,$email,$notifications,$data));
	}


	private function send_mail_edit($id,$notifications)
	{

		$from=$this->_from;
		$model = new NotificationsHistory();
		$email=$model->getEmail('nh.id_notification='.$id);
		$data=$model->getLastMessage($id);

		$notifications['city']=$this->_get_city_from_unit($notifications);
		//echo 'CITY='.$notifications['city'];

		//print("Wynik notification:<BR><pre>");
		//print_r($notifications);
		//print("<BR><pre>") ;

		//print("Wynik email:<BR><pre>");
		//print_r($email);
		//print("<BR><pre>") ;

		//print("Wynik data:<BR><pre>");
		//print_r($data);
		//print("<BR><pre>") ;

		return ($this->send_mail($from,$email,$notifications,$data));
	}


	private function send_mail($from,$email,$notifications,$data)
	{
		require 'libs/PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

		foreach ($email as $r) {
            $mail->addAddress($r['email']);
        }

			# Smtp
            $mail->IsSMTP();
            $mail->Host = "smtp.mcz.pl";
            $mail->Username = "sendermcz@mcz.pl";
            $mail->Password = "S3d4rlokal";

            $mail->SMTPAuth = true;
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';

			$mail->CharSet = "UTF-8";
			$mail->From = 'sendermcz@mcz.pl';
			$mail->FromName = 'Aplikacja wew. MCZ';

			$mail->isHTML(true);

			$mail->Subject = 'Zgłoszenie MCZ id='.$notifications['id'];
			$mail->Body    = "

				<table border=\"1\" width=\"600\" cellspacing=\"1\" cellpadding=\"3\">
				<tr><th align=\"left\" width=\"180\">ID zgłoszenia:</th><td>".$notifications['id']."</td></tr>
				<th align=\"left\">Imię zgłaszającego:</th><td>".$notifications['name']."</td></tr>
				<th align=\"left\">Nazwisko zgłaszającego:</th><td>".$notifications['surname']."</td></tr>
				<th align=\"left\">Temat zgłoszenia:</th><td>".$notifications['subject']."</td></tr>
				<th align=\"left\">Data zgłoszenia:</th><td>".date('Y-m-d H:i:s',$notifications['add_date'])."</td></tr>
				<th align=\"left\">Miasto:</th><td>".$notifications['city']."</td></tr>
				<th align=\"left\">Jednostka:</th><td>".$notifications['unit']."</td></tr>
				<th align=\"left\">Wiadomość:</th><td>".$data['description']."</td></tr>
				<th align=\"left\">Przyczyna powstania:</th><td>".$notifications['reason']."</td></tr>
				<th align=\"left\">Typ:</th><td>".$notifications['type']."</td></tr>
				<th align=\"left\">Priorytet:</th><td>".$notifications['priority']."</td></tr>
				<th align=\"left\">Status:</th><td>".$notifications['status']."</td></tr>
				</table>";

			$debug = 0;

            $mail->SMTPDebug = $debug;

            try {
                if (!$mail->send()) {

                    if ($debug === 0) {

                        return false;
                    }

                    echo $mail->ErrorInfo;
                    exit;

                }
			} catch (Exception $e) {
                if ($debug === 0) {

                    return false;
                }

            	 var_dump($e); die;
			}

			return true;

//		die();
//		foreach ($email as $r)
//		{
//			$email_to=$r['email'];
//			$email_to = 'patrycjusz@evipstudio.pl';
//
//
//			//$headers = "From: ".$from." \r\n"
//			//		  . "Content-Type: text/plain; charset=UTF-8; format=flowed\n"
//			//		  . "MIME-Version: 1.0\n"
//	    	//		  . "Content-Transfer-Encoding: 8bit\n"
//	        //         . "X-Mailer: PHP\n";
//	        //
//	        $body = "
//
//			<table border=\"1\" width=\"600\" cellspacing=\"1\" cellpadding=\"3\">
//			<tr><th align=\"left\" width=\"180\">ID zgłoszenia:</th><td>".$notifications['id']."</td></tr>
//			<th align=\"left\">Imię zgłaszającego:</th><td>".$notifications['name']."</td></tr>
//			<th align=\"left\">Nazwisko zgłaszającego:</th><td>".$notifications['surname']."</td></tr>
//			<th align=\"left\">Temat zgłoszenia:</th><td>".$notifications['subject']."</td></tr>
//			<th align=\"left\">Data zgłoszenia:</th><td>".date('Y-m-d H:i:s',$notifications['add_date'])."</td></tr>
//			<th align=\"left\">Miasto:</th><td>".$notifications['city']."</td></tr>
//			<th align=\"left\">Jednostka:</th><td>".$notifications['unit']."</td></tr>
//			<th align=\"left\">Wiadomość:</th><td>".$data['description']."</td></tr>
//			<th align=\"left\">Przyczyna powstania:</th><td>".$notifications['reason']."</td></tr>
//			<th align=\"left\">Typ:</th><td>".$notifications['type']."</td></tr>
//			<th align=\"left\">Priorytet:</th><td>".$notifications['priority']."</td></tr>
//			<th align=\"left\">Status:</th><td>".$notifications['status']."</td></tr>
//			</table>";
//
//			require 'public/emails/newsletter.php';
//
//			$mail = new Cube_Mail();
//			$mail->is_html = $html;
//			//$mail->attach = $attach;
//			//$mail->types = $types;
//			//$mail->names = $names;
//
//
//
//			//print_r ($content);
//			//if(!(@mail($email_to, 'Zgłszenie id='.$notifications['id'], $content, $headers)))
//			if(!($mail->send($from, $email_to, 'Zgłoszenie MCZ id='.$notifications['id'], $content)))
//				return false;
//
//		}
		return true;
	}

	private function create_pdf($body,$filename)
	{
		//dodaj szblon
		require 'public/ftp/newsletter.php';  //zwraca html i dodaje body

		require_once 'libs/Mpdf/mpdf.php'; // ładujemy klasę
		$mpdf=new mPDF('utf-8','A4','',''); // ustawiamy parametry strony
		$mpdf->SetCompression(true); // włączamy kompresję
		$mpdf->SetProtection(array('print')); // Uprawnienia dokumentu
		//$mpdf->SetTitle("Tytuł dokumentu"); // Podajemy tytuł dokumentu
		//$mpdf->SetAuthor("CodeTip"); // Wprowadzamy autora
		//$mpdf->SetWatermarkText("CodeTip - Porady i Sztuczki"); // umieszczamy znak wodny
		//$mpdf->showWatermarkText = false; // Pokazujemy lub nie, znak wodny powyżej
		//$mpdf->watermark_font = 'DejaVuSansCondensed'; // Czcionka znaku wodnego
		//$mpdf->watermarkTextAlpha = 0.05; // Przeźroczystość znaku wodnego
		$mpdf->SetDisplayMode('fullpage'); // Typ wyświetlenia
		$mpdf->WriteHTML($html); // Przetworzenie treści zmiennej $html*/

		$mpdf->Output($filename.'.pdf','D'); // Wygenerowanie i wysłanie do użytkownika pliku


	}
private function check_date(&$start_date,&$end_date)
{
	if($start_date != '')
	{
		if (!preg_match('/^(\d\d)-(\d\d)-(\d\d\d\d)$/',$start_date,$wynik))
			$this->_request->redirectFailure(array('Podano nieprawidłowy format daty poczatku.'));
		if($wynik[1]>31)
			$this->_request->redirectFailure(array('Podano nieprawidłowy dzień w polu data poczatku.'));
		if($wynik[2]>12)
			$this->_request->redirectFailure(array('Podano nieprawidłowy miesiac w polu data poczatku.'));
		if($wynik[3]<1900)
			$this->_request->redirectFailure(array('Podano nieprawidłowy rok w polu data poczatku.'));

		$start_date=mktime(0,0,0,$wynik[2],$wynik[1],$wynik[3]);
	}

	if($end_date != '')
	{
		if (!preg_match('/^(\d\d)-(\d\d)-(\d\d\d\d)$/',$end_date,$wynik))
			$this->_request->redirectFailure(array('Podano nieprawidłowy format daty końca.'));
		if($wynik[1]>31)
			$this->_request->redirectFailure(array('Podano nieprawidłowy dzień w polu data końca.'));
		if($wynik[2]>12)
			$this->_request->redirectFailure(array('Podano nieprawidłowy miesiac w polu data końca.'));
		if($wynik[3]<1900)
			$this->_request->redirectFailure(array('Podano nieprawidłowy rok w polu data końca.'));

		$end_date=mktime(23,59,59,$wynik[2],$wynik[1],$wynik[3]);
	}
}
	public function init()
	{
		//$this->_categoriesModel = new AdvGalleryCategories();
		$this->_pp = $this->_config->get('per_page', 'advgallery');
		$this->_from = $this->_config->get('email', 'notifications');

		$this->view->id = $this->_id = $this->_request->getParam('id', 0);

		$this->view->column = $this->_column = $this->_request->getParam('column');
		$this->view->state = $this->_state = $this->_request->getParam('state');
		$search_string = explode( STRING_SEPARATOR ,$this->_request->getParam('search'));

		$this->view->search=$this->_search=$search_string[0];
		//$this->view->search=$this->_search=str_replace('&oacute;', 'ó', $search_string[0]);
		//echo 'SEARCH'.$this->view->search;
		$this->view->start_date=$this->_start_date=$search_string[1];
		$this->view->end_date=$this->_end_date=$search_string[2];
		$this->view->print=NULL;
		$this->view->csv=NULL;
	}

public function csv_indexAction()
	{

		$dane=null;	//zmienna w ktorej jest zapisana tresc zapisana do pliku csv
		$this->view->render('index');
		$this->sort_prepare();
		$this->sortAction();

		$rows = (array)$this->view->rows;
		if (count($rows) < 1 )
		{
			header('refresh: 3; url=zgloszenia.html');
			$this->view->message = 'Brak danych do utworzenia pliku csv. Przekierowywanie...';
			return;
		}
		else
		{
			$dane.="\"ID\";\"LOGIN\";\"IMIĘ\";\"NAZWISKO\";\"TEMAT\";\"STATUS\";\"TYP\";\"PRIORYTET\";\"JEDNOSTKA\";\"DATA DODANIA\";\r\n";
			$dane=iconv("UTF-8","windows-1250",$dane);
			foreach($rows as $r)
			{
				$id=$r['id'];
				//$login=$r['login'];
				$login=str_replace('&oacute;', 'ó', $r['login']);
				$login=iconv("UTF-8","windows-1250",$login);

				//$name=$r['name'];
				$name=str_replace('&oacute;', 'ó', $r['name']);
				$name=iconv("UTF-8","windows-1250",$name);

				//$surname=$r['surname'];
				$surname=str_replace('&oacute;', 'ó', $r['surname']);
				$surname=iconv("UTF-8","windows-1250",$surname);

				//$subject=$r['subject'];
				$subject=str_replace('&oacute;', 'ó', $r['subject']);
				$subject=iconv("UTF-8","windows-1250",$subject);

				//$status=$r['status'];
				$status=str_replace('&oacute;', 'ó', $r['status']);
				$status=iconv("UTF-8","windows-1250",$status);

				//$type=$r['type'];
				$type=str_replace('&oacute;', 'ó', $r['type']);
				$type=iconv("UTF-8","windows-1250",$type);

				//$priority=$r['priority'];
				$priority=str_replace('&oacute;', 'ó', $r['priority']);
				$priority=iconv("UTF-8","windows-1250",$priority);

				//$unit=$r['unit'];
				$unit=str_replace('&oacute;', 'ó', $r['unit']);
				$unit=iconv("UTF-8","windows-1250",$unit);

				$add_date=date('Y-m-d',$r['add_date']);
				$dane.="\"$id\";\"$login\";\"$name\";\"$surname\";\"$subject\";\"$status\";\"$type\";\"$priority\";\"$unit\";\"$add_date\";\r\n";
			}
			//print("Wynik data:<BR><pre>");
			//print_r($rows);
			//print("<BR><pre>") ;
		}

		//zapisz dane do pliku
		$fp=NULL;
		$path  = CSV_DIR;
		//$dane="Test\"$path\"\r\nNowa linia;";
		$file="Lista_zgloszen.csv";
		// uchwyt pliku, otwarcie do zapisu

		if (!($fp=fopen("$path$file", "w")))
		{
			header('refresh: 3; url=zgloszenia.html');
			$this->view->message = 'Nie mozna otworzyc pliku. Przekierowywanie...';
			return;
		}
		//blokada pliku
		flock($fp, 2);
		fwrite($fp, $dane);
		// odblokowanie pliku
		flock($fp, 3);
		fclose($fp);

		//header('refresh: 3; url=admin,notifications.html');
		//$this->view->render('index');
		//$this->view->message = 'Plik csv utworzony. Przekierowywanie...';

		$this->view->csv=1;
        //to przekierowanie jest potrzebne poniewaz w przypadku blednego wpisania daty, a pozniej poprawnego nie ma powrotu na notifications_index.
         header('refresh: 1; url=zgloszenia.html');
         $this->view->render('index');
         //$this->view->message = 'Plik csv utworzony. Przekierowywanie...';
		//return;

	}

	public function pdf_indexAction()
	{
		$body='';
		$this->view->render('index');

		$this->sort_prepare();
		$this->sortAction();

		//$this->indexAction();
		$body='
		<table cellspacing="1"  cellpadding="3" border="1" width="100%">
			<thead>
				<tr>
					<td class="tocenter">ID</td>
					<td>Tytuł</td>
					<td>Status</td>
					<td>Typ</td>
					<td>Priorytet</td>
					<td>Jednostka</td>
					<td class="tocenter">Data</td>
				</tr>
			</thead>
			<tbody>';


			$rows = (array)$this->view->rows;
			if (count($rows) < 1 ) {
				$body.='<tr><td colspan="4">Nie odnaleziono żadnych wpisów</td></tr>';
			} else {
				foreach($rows as $r)
				{
					$class = getTableClass();
					$body.= '<tr'.$class.'>
						<td class="tocenter">'.$r['id'].'</td>
						<td>'.$r['subject'].'</td>
						<td>'.$r['status'].'</td>
						<td>'.$r['type'].'</td>
						<td>'.$r['priority'].'</td>
						<td>'.$r['unit'].'</td>
						<td class="tocenter">'.date('Y-m-d',$r['add_date']).'</td>
					</tr>';


				}
			}
		$body.='</tbody></table>';
		$filename='Lista zgłoszeń';
		$this->create_pdf($body,$filename);
	}

	public function print_indexAction()
	{
		//drukuj liste zgloszen
		$this->view->render('index');
		$this->view->print=1;

		$this->sort_prepare();
		$this->sortAction();

		//$this->indexAction();
	}


	public function indexAction()
	{
		$model = new Notifications();
		$id_user=$this->_session->getUserId();
		$this->view->rows = $model->getNotifications('n.id_user='.$id_user,'id desc');
		$this->paging($this->view->rows);
	}




	public function searchAction()
	{
		if ($this->_request->isRedirected())
		{
			$this->view->render('index');
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest();
			//print_r($this->view->row);
			//$this->view->id = $this->view->row['id'];
			//echo 'END_DATE='.$this->view->end_date;
			return;
		}

		if ($this->_request->isPost())
		{
			$search=clear($_POST['search']);
			$start_date = clear($_POST['start_date']);
			$end_date = clear($_POST['end_date']);

			$this->view->start_date = $this->_start_date = $start_date;
			$this->view->end_date = $this->_end_date = $end_date;
			$this->view->search = $this->_search = $search;

			$this->check_date($start_date,$end_date);
			$model = new Notifications();
			//$where='p.title like "%'.$search.'%"';

			$where=$this->create_where($search,$start_date,$end_date);
			//echo 'where ='. $where;
			$id_user=$this->_session->getUserId();
			$model = new Notifications();
			$this->view->rows = $model->getNotifications('n.id_user='.$id_user.' AND '.$where,'id desc');
			//print_r($this->view->rows);
			if (sizeof($this->view->rows) < 1)
			{
				//$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
				//echo "Jestem w IF";
				header('refresh: 3; url=zgloszenia.html');
				$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
				$this->view->render('index');
				//return;
			}
			$this->view->render('index');
			$this->paging($this->view->rows);
		}
	}


	public function sortAction()
	{
		//na potrzeby stronicowania, gdy columna lub stan sa puste
		$tmp_column=$this->_column;
		$tmp_state=$this->_state;

		if( ( $this->_column =='' || !is_numeric($this->_column)) ||
		    ( $this->_state ==''  || !is_numeric($this->_state )) )
		{
			$this->_column=0;	//kolumna z ID lub pos.
			$this->_state=2;	//gdy domyslenie ma byc sortowane w gore, w innym przypadku ustawic na 1.
		}

		switch ($this->_column)
		{
			case 0: $this->sortID(); break;
			case 1: $this->sortTitle(); break;
			case 2: $this->sortStatus(); break;
			case 3: $this->sortType(); break;
			case 4: $this->sortPriority(); break;
			case 5: $this->sortUnit(); break;
			case 6: $this->sortDate(); break;
		}

		$this->_column =$tmp_column;
		$this->_state=$tmp_state;

		//gdy sortowanie jest rowniez uzywane do PDF lub wydruku. IF gdy nie chcemy aby PDF lub wydruk byly stronicowane
		if((!strcmp($this->_request->getAction(),'sort'))||
		   (!strcmp($this->_request->getAction(),'search'))||
		   (!strcmp($this->_request->getAction(),'csv')) )
			$this->paging($this->view->rows);
		//print_r($this->view->rows);

	}


	public function pdfeditAction()
	{
		//pdf - szczegóły zgłoszenia
		$this->view->render('edit');
		//$this->view->message = 'Nie udało się wysłać wiadomośi z nieokreślonych przyczyn. Spróbuj ponownie za kilka chwil.';
		//exit;
		//$model = new NotificationsStatus();
		//$citys=$this->view->status = $model->getAll(null,'pos','id');

		//$model = new NotificationsPriority();
		//$citys=$this->view->prioritys = $model->getAll(null,'pos','id');

		$model = new Notifications();
		$rows = $model->getNotifications('n.id='.$this->_id);
		//print_r($rows);
		$notifications=$this->view->notifications=$rows[0];
		//powierz pierwsza wiadomosc
		$model = new NotificationsHistory();
		$first_message = $model->getFirstMessage($this->_id);
		//print_r($first_message);
		$this->view->notifications['description']=$first_message['description'];
		//pobierz wszystkie wiadomosci
		$this->view->messages=$model->getNotificationsHistory('nh.id_notification='.$this->_id,'id');


		$n = (array)$this->view->notifications;

		if (strcmp($n['login'],$this->_session->getUsername()))
		{
			//wyjdz jezeli inny user niz na zgloszeniu
			return;
		}

		$body='';

		$body.= '<table cellspacing="1"  cellpadding="3" border="1" width="100%">
		  <thead>
			<tr><td colspan="2">Dane o zgłoszeniu</td></tr>
		  </thead>
		  <tbody>
			<tr><td width="180">Jednostka organizacyjna:</td><td>'.$n['unit'].'</td></tr>
			<tr><td>Temat:</td><td>'.$n['subject'].'</td></tr>
			<tr><td>Przyczyna powstania:</td><td><div style="overflow:auto; height:100px;">'.$n['reason'].'</div></td></tr>
			<tr><td>Priorytet:</td><td>'.$n['priority'].'</td></tr>
			<tr><td>Status:</td><td>'.$n['status'].'</td></tr>
			<tr><td>Typ:</td><td>'.$n['type'].'</td></tr>
			<tr><td>Data dodania:</td><td>'.date('Y-m-d H:i:s',$n['add_date']).'</td></tr>
			<tr><td>Opis:</td><td><div style="overflow:auto; height:100px;">'.$n['description'].'</div></td></tr>
		  </tbody>
		  </table>
		 <table cellspacing="1"  cellpadding="3" border="1" width="100%">
		  <thead>
			<tr><td colspan="2">Użytkownik zgłaszający</td></tr>
		  </thead>
		  <tbody>
			<tr><td width="80">Login:</td><td>'.$n['login'].'</td></tr>
			<tr><td>Imię:</td><td>'.$n['name'].'</td></tr>
			<tr><td>Nazwisko:</td><td>'.$n['surname'].'</td></tr>
		  </tbody>
		  </table>
		  <br clear="all" />';


		$body.= '<h3>Historia zgłoszenia</h3>';
		$rows = (array)$this->view->messages;
		foreach($rows as $r)
		{
			$body.= '<table cellspacing="1"  cellpadding="3" border="1" width="100%">
			 <tr><td>'.$r['login'].' - ('.date('Y-m-d H:i:s',$r['add_date']).')</td></tr>
			  <tr><td>'.$r['description'].'</td></tr>
			  </table>';



		}
		$filename='Zgloszenie_nr '.$this->_id;
		$this->create_pdf($body,$filename);
	}

	public function printeditAction()
	{
		//drukuj liste zgloszen
		$this->view->render('edit');
		//$this->view->message = 'Nie udało się wysłać wiadomośi z nieokreślonych przyczyn. Spróbuj ponownie za kilka chwil.';
		//exit;
		$this->view->print=1;
		$this->editAction();
	}


	public function editAction()
	{
		$model = new NotificationsStatus();
		$citys=$this->view->status = $model->getAll(null,'pos','id');

		$model = new NotificationsPriority();
		$citys=$this->view->prioritys = $model->getAll(null,'pos','id');

		$model = new Notifications();
		$rows = $model->getNotifications('n.id='.$this->_id);
		//print_r($rows);
		$notifications=$this->view->notifications=$rows[0];
		//powierz pierwsza wiadomosc
		$model = new NotificationsHistory();
		$first_message = $model->getFirstMessage($this->_id);
		//print_r($first_message);
		$this->view->notifications['description']=$first_message['description'];
		//pobierz wszystkie wiadomosci
		$this->view->messages=$model->getNotificationsHistory('nh.id_notification='.$this->_id,'id');

		//echo '<div style="position:fixed; bottom:0; left:0; width:100px; height:100px; background:#f00;">';
		//echo 'START='.$start;
		//echo '</div>';


		if ($this->_request->isRedirected())
		{
			$this->view->render('edit');
			$this->view->message = 'Nie udało się wysłać wiadomośi z nieokreślonych przyczyn. Spróbuj ponownie za kilka chwil.';
			exit;
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest();
			//$this->view->id=$this->view->row['id'];
			//$model = new Notifications();
			//$rows = $model->getNotifications('n.id='.$this->view->id);
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new NotificationUserHistoryFilter();
			$filter->filter();
			$data = $filter->getData();

			//dla usera skopiuj priorytet i status z zglszenia
			$data['id_priority']=$notifications['id_priority'];
			$data['id_status']=$notifications['id_status'];

			$model = new NotificationsHistory();
			$data['id_user']=$this->_session->getUserId();
			$data['id_notification']=$this->_id;
			//print_r($data);
			$model->insert($data);

			//aktualizuj status i priorytet
			$model = new Notifications();
			$data_n['id_priority']=$data['id_priority'];
			$data_n['id_status']=$data['id_status'];
			$model->update($this->_id, $data_n);

			//wyslanie emaila
			if( $this->send_mail_edit($this->_id,$notifications) )
			{
				$this->view->render('edit');
				header('refresh: 3; url=zgloszenia_'.$this->_id.'.html');
				$this->view->message = 'Odpowiedz dodana pomyślnie ! Wiadomość wysłana. Przekierowywanie...';
			}
			else
			{
				$this->view->render('edit');
				header('refresh: 3; url=zgloszenia_'.$this->_id.'.html');
				$this->view->message = 'Nie udało się wysłać wiadomośi z nieokreślonych przyczyn. Spróbuj ponownie za kilka chwil.';
			}

			//$this->view->render('edit');
			//header('refresh: 3; url=zgloszenia.html');
			//$this->view->message = 'Odpowiedz dodana pomyślnie ! Przekierowywanie...';
		}
	}




	public function addAction()
	{
		$model = new NotificationsType();
		$citys=$this->view->types = $model->getAll(null,'pos','id');

		$model = new NotificationsPriority();
		$citys=$this->view->prioritys = $model->getAll(null,'pos','id');


		$model = new NotificationsCity();
		$citys=$this->view->citys = $model->getAll(null,'pos','id');
		//print_r($this->view->citys);
		//wybrane miasto
		$this->view->city=$citys[0]['title'];
		$id=$citys[0]['id'];
		$model = new NotificationsUnit();
		$this->view->units = $model->getUnits('u.id_city='.$id.'','u.pos','*,c.title as city');
		//echo 'CZESC GLOWNA';

		if($this->_request->isGet() && ($this->_id !='' && is_numeric($this->_id)))
		{
			//echo 'GET';
			//pobierz wybrane miasto
			$model = new NotificationsCity();
			$rows=$model->get($this->_id);
			$this->view->city=$rows['title'];

			//pobierz jednostki dla miasta
			$model = new NotificationsUnit();
			$this->view->units = $model->getUnits('u.id_city='.$this->_id.'','u.pos','*,c.title as city');
			//$this->view->render('add');
		}

		if ($this->_request->isRedirected())
		{
			//echo 'REDIRECT';
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest();
			//$this->view->id = $this->view->row['id'];
			return;
		}
		//header('refresh: 3; url=zgloszenia.html');
		//	$this->view->message = 'Nowy typ zdarzenia dodany pomyślnie ! Przekierowywanie...';
		if ($this->_request->isPost())
		{
			//echo 'POST';
			$filter = new NotificationAddFilter();
			$filter->filter();
			$data = $filter->getData();

			$model = new Notifications();
			$data['id_user']=$this->_session->getUserId();
			$id_notification_history=$model->insert($data);
			//echo 'ID='.$id_notification_history;
			//wyslanie emaila
			if( $this->send_mail_add($id_notification_history))
			{
				$this->view->render('edit');
				header('refresh: 3; url=zgloszenia.html');
				$this->view->message = 'Nowy typ zdarzenia dodany pomyślnie ! Wiadomość wysłana. Przekierowywanie...';
			}
			else
			{
				$this->view->render('edit');
				header('refresh: 3; url=zgloszenia.html');
				$this->view->message = 'Nie udało się wysłać wiadomośi z nieokreślonych przyczyn. Spróbuj ponownie za kilka chwil.';
			}
			//header('refresh: 3; url=zgloszenia.html');
			//$this->view->message = 'Nowy typ zdarzenia dodany pomyślnie ! Przekierowywanie...';

		}
	}

	public function addonchangeAction()
	{
		$model = new NotificationsUnit();
		$this->view->units = $model->getUnits('u.id_city='.$this->_id.'','u.pos','*,c.title as city');
		$this->view->render('add');
	}
}

?>
