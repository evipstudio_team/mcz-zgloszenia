<?php 

class Admin_ArticlestocController extends Cube_Controller_Abstract
{
	private $_act = null;
	private $_id = null;
	
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->_act = $this->_request->getParam('act');	
		$this->_id = $this->_request->getParam('id');	
		$this->view->id = $this->_id;
	}
	
	
	public function indexAction()
	{
		$model = new ArticlesTOC();
		$this->view->rows = $model->getAll(null, 'language, id DESC');	
	}

	public function addAction()
	{		
		$model = new ArticlesTOC();
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new ArticlesTOCFilter();
			$filter->filter();			
			//$model = new Articles();
			$model->insert($filter->getData());	
			header('refresh: 3; url=admin,articlestoc.html');
			$this->view->message = 'Tabela zawartości dodana pomyślnie ! Przekierowywanie...';
		}
	}

	public function editAction()
	{
		$model = new ArticlesTOC();
		$row = $model->get($this->_id);
		$this->view->row = $row;
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new ArticlesTOCFilter();
			$filter->filter();
			$model->update($this->_id, $filter->getData());
			header('refresh: 3; url=admin,articlestoc.html');
			$this->view->message = 'Tabela zawartości zaktualizowana pomyślnie ! Przekierowywanie...';	
		}
	}

	public function deleteAction()
	{
		$model = new ArticlesTOC();
		$model->delete($this->_id);
		header('refresh: 3; url=admin,articlestoc.html');
		$this->view->message = 'Tabela zawartości usunięta pomyślnie ! Przekierowywanie...';
		$this->view->render('edit');
	}	
			
}

?>
