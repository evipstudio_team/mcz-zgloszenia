<?php 

class Admin_AdvgalleryController extends Cube_Controller_Abstract
{
	private $_id;
	private $_cid;
	private $_pid;	
	private $_categoriesModel;
	
	private $_id_state=null;
	private $_title_state=null;
	private $_adddate_state=null;
	private $_filename_state=null;
	private $_category_state=null;
	private $_search=null;
	private $_category_id=null;
	
	
	
	private function create_where($search)
	{
		$where_id='p.id like "%'.$search.'%"';
		$where_title='p.title like "%'.$search.'%"';
		$where_filename='p.filename like "%'.$search.'%"';
		$where='('.$where_title.' OR '.$where_id.' OR '.$where_filename.')';
		
		//dolaczenie kategorii
		//if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		//else 
			$cid = $this->_request->getParam('categoryid', 0);
			$this->view->cid = $cid;
		
		//echo 'CID WHERE='.$cid;
		//echo 'CATEGORY ID='.$this->_category_id;
		
		if ($cid > 0) 
			$where=$where.' AND p.cid = "'.$cid.'"';
		$this->view->categoriesList = $this->_getCategoriesList($cid);	
		return $where;
		
	}
	
	private function sort($field,$sort)
	//public function sortLoginAction()
	{
		
		$model = new AdvGallery();
		//$field='Login';
		
		
		if ($sort==1)
			$order='ASC';
		elseif ($sort==2)
			$order='DESC';
	
	
		$where=$this->create_where($this->_search);
		
		$this->view->rows = $model->getAll($where,''.$field.' '.$order.'');
		//sort($order, $field);
		$this->view->render('index');
		
	}
		
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->view->username = $this->_session->getUsername();
		$this->_id = $this->_request->getParam('id', 0);
		$this->view->id = $this->_id;
		$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->cid = $this->_cid;	
				
		if (isset($_POST['pid']) && $_POST['pid'] > 0) {
			$this->view->pid = $this->_pid = (int)$_POST['pid'];
		} else {
			$this->_pid = $this->_request->getParam('pid', 1);
			$this->view->pid = $this->_pid;	
		}	
		$this->view->controller = $this->_request->getController();	
		$this->_categoriesModel = new AdvGalleryCategories();
		$this->view->categoriesList = $this->_getCategoriesList();
		
		//sort i search
		$this->view->id_state = $this->_id_state = $this->_request->getParam('idstate');
		$this->view->title_state = $this->_title_state = $this->_request->getParam('titlestate');	
		$this->view->adddate_state = $this->_adddate_state = $this->_request->getParam('adddatestate');
		$this->view->filename_state = $this->_filename_state = $this->_request->getParam('filenamestate');	
		$this->view->category_state = $this->_id_category = $this->_request->getParam('categorystate');
		$this->view->search = $this->_search = $this->_request->getParam('search');	
		$this->view->categoryid = $this->_category_id = $this->_request->getParam('category_id');
	}

	# zwraca tablice postaci: id => sciezka typu Top/Test/Subtest, gdzie ID to nr id kategorii subtest
	private function _getTree($parent_id, $current_name, &$tree)
	{	
		$subcats = $this->_categoriesModel->getSubcategories($parent_id);
		if (count($subcats) < 1)
			return $tree;
			
		foreach ($subcats as $sub)
		{
			$tree[$sub['id']] = $current_name.$sub['name'].'/';
			$this->_getTree($sub['id'], $tree[$sub['id']], $tree);	
		}	
		
		return $tree;
	}

	public function _getCategories()
	{
		$tree = array(1 => 'Korzeń/');
		$tree = (array)$this->_getTree(1, 'Korzeń/', $tree);
		asort($tree);
		
		//print_r($tree);
		return $tree;	
	}

	public function _getCategoriesList($cid = null, $no = false)
	{
		$tree = array(1 => 'Korzeń/');
		$tree = (array)$this->_getTree(1, 'Korzeń/', $tree);
		asort($tree);
		
		$temp = '';
		foreach ($tree as $id => $name)
		{
			if (!is_null($cid) && $id == $cid) {
				if (!$no) $temp .= '<option value="'.$id.'" selected="selected">'.$name.'</option>';
			} else $temp .= '<option value="'.$id.'">'.$name.'</option>';
		}
		return $temp;	
	}

	protected function _getSubCategories($parent_id)
	{
		$cats = $this->_categoriesModel->getSubcategories($parent_id);
		return $cats;
	}

	public function indexAction()
	{		
		$model = new AdvGallery();
		
		
		
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else 
			$cid = $this->_cid;
		
		//potrzebne dla szablonu	
		$this->view->cid = $cid;		
		//echo 'INDEX CID=',$cid;	
		
		if ($cid > 0) {
			$this->view->rows = $model->getAll('p.cid = "'.$cid.'"', 'p.id DESC');		
		} else 	$this->view->rows = $model->getAll(null, 'p.cid DESC, p.id DESC');
		$this->view->categoriesList = $this->_getCategoriesList($cid);
	}	
	
	public function searchAction()
	{
		$search=$_POST['search'];
		//$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
		$model = new AdvGallery();
		//$where='p.title like "%'.$search.'%"';
		
		$where=$this->create_where($search);
		$this->view->rows = $model->getAll($where,null);
		if (sizeof($this->view->rows) < 1)
		{
			//$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
			//echo "Jestem w IF";
			header('refresh: 3; url=admin,advgallery.html');
			$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
			$this->view->render('index');
			
								
			//$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
			//$this->view->render('order');
			//$this->redirect(3, null, 'index');	
			return;
		}
		$this->view->render('index');
	}
		
	public function sortCategoryAction()
	{
		//$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
		$field='c.name';
		$sort=$this->_title_state;
		$this->sort($field,$sort);		
	}
	
	
	public function sortFielNameAction()
	{
		$field='p.filename';
		$sort=$this->_author_state;
		$this->sort($field,$sort);		
	}
	
	public function sortAddDateAction()
	{
		$field='p.add_date';
		$sort=$this->_adddate_state;
		$this->sort($field,$sort);		
	}
		
	
	public function sortTitleAction()
	{
		$field='p.title';
		$sort=$this->_title_state;
		$this->sort($field,$sort);		
	}
	
	public function sortIDAction()
	{
		$field='p.id';
		$sort=$this->_id_state;
		$this->sort($field,$sort);		
	}
	
	public function insertAction()
	{			
		$this->view->render('index');
				
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->category 	  = $params['cid'];
			$this->view->title 	  = $params['title'];
			$this->view->description = $params['description'];	
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);				
			return;
		}
	
		$filter = new AdvGalleryFilter();
		$filter->filter();	
		$data = $filter->getData();
		$this->view->categoriesList = $this->_getCategoriesList($data['cid']);
		
		
		$path = $this->_categoriesModel->getPath($data['cid']);
		
		if ($path['path'] == '/')
			$path['path'] = '';
		
		// NEW IMAGE ====================
		$pathTemp = ADVGALLERY_DIR.'temp/';
		$pathBig  = ADVGALLERY_DIR.$path['path'];
		$pathThumb= ADVGALLERY_DIR.$path['path'].'thumbs/'; 
		
		Cube_Loader::loadClass('Cube_Upload_Image');
		$image = new Cube_Upload_Image('photo');
		$image->setPath($pathTemp);
		$image->setMaxSize(10240);
		$image->setSaveAs(':name');	
		$image->move();	

		if ($image->errorExists()) $this->_request->redirectFailure($image->getErrors());	
		$filename = $image->getSaveName();

		if (file_exists($pathBig.$filename)) $this->_request->redirectFailure(array('Już istnieje w bazie zdjęcie o takiej nazwie pliku ('.$filename.').'));

		// jesli fota jest wieksza niz moze byc
		if ($image->getX() > 800 || $image->getY() > 600) {
			if (!$image->resize(800, 600, $pathBig)) $this->_request->redirectFailure(array('Nie udało się zmniejszyć zdjęcia do rozmiarów 800x600.'));
		} else rename($pathTemp.$filename, $pathBig.$filename);	
		// tworzy miniaturkę
		
		if ($image->getX() > ADVGALLERY_THUMB_WIDTH || $image->getY() > ADVGALLERY_THUMB_HEIGHT) {
			if (!$image->resize(ADVGALLERY_THUMB_WIDTH, ADVGALLERY_THUMB_HEIGHT, $pathThumb)) $this->_request->redirectFailure(array('Nie udało się utworzyć miniaturki.'));
		}
		if ($image->getX() > ADVGALLERY_THUMB2_WIDTH || $image->getY() > ADVGALLERY_THUMB2_HEIGHT) {
			if (!$image->resize(ADVGALLERY_THUMB2_WIDTH, ADVGALLERY_THUMB2_HEIGHT, $pathThumb.'mini/')) $this->_request->redirectFailure(array('Nie udało się utworzyć miniaturki.'));
		}					
		// usuwa tymczasowe zdjecie
		$image->deleteImage();

		$data['filename'] = $filename;
		$model = new AdvGallery();	
		$model->insert($data);	
		header('refresh: 3; url=admin,advgallery,redirect.html');
		$this->view->message = 'Wpis dodany pomyślnie ! Przekierowywanie...';		
	} 
	
	public function redirectAction()
	{
		header('Location: admin,advgallery.html');
	}
	
	public function redirectsettAction()
	{
		header('Location: admin,advgallery,settings.html');
	}	
	
	public function editAction() 
	{
		$model = new AdvGallery();
		$row = $model->get($this->_id);
		$this->view->id 	  = $row['id'];
		$this->view->category 	  = $row['cid'];
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);
		$this->view->title 	  = $row['title'];
		$this->view->description = $row['description'];
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->category 	  = $params['cid'];
			$this->view->title 	  = $params['title'];
			$this->view->description = $params['description'];	
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);							
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new AdvGalleryFilter();
			$filter->filter();
			$model->update($this->_id, $filter->getData());
			header('refresh: 3; url=admin,advgallery.html');
			$this->view->message = 'Wpis zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}	
	
	public function deleteAction()
	{
		$model = new AdvGallery();
		$row = $model->get($this->_id);	
		if (file_exists(ADVGALLERY_DIR.$row['path'].$row['filename'])) 
				unlink(ADVGALLERY_DIR.$row['path'].$row['filename']);
		if (file_exists(ADVGALLERY_DIR.$row['path'].'thumbs/'.$row['filename'])) 
				unlink(ADVGALLERY_DIR.$row['path'].'thumbs/'.$row['filename']);
		if (file_exists(ADVGALLERY_DIR.$row['path'].'thumbs/mini/'.$row['filename'])) 
				unlink(ADVGALLERY_DIR.$row['path'].'thumbs/mini/'.$row['filename']);		
		$model->delete($this->_id);
		header('refresh: 3; url=admin,advgallery.html');
		$this->view->message = 'Wpis usunięty pomyślnie ! Przekierowywanie...';
		$this->view->render('edit');
	}	
	
	// =====================
	public function categoriesAction()
	{
		$this->view->rows = $this->_getSubcategories($this->_pid);		
		$cats =  $this->_getCategories();
		$this->view->currentPath = $cats[$this->_pid];
		$this->view->current = $this->_categoriesModel->get($this->_pid);
	}

	public function addcatAction()
	{
		$this->view->name = null;
		$this->view->render('categories');	
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->name   = $params['name'];		
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new AdvGalleryCategoryFilter();
			$filter->filter();			
			$model = new AdvGalleryCategories();
			$data = $filter->getData();
			
			$parent = $this->_categoriesModel->get($data['pid']);
			$data['path'] = profileName($parent['path'].$data['name'].'/');
			$data['parent_id'] = $data['pid'];
			unset($data['pid']);
			
			if (!file_exists(ADVGALLERY_DIR.$data['path']))
				mkdir(ADVGALLERY_DIR.$data['path'], 0777);
				
			if (!file_exists(ADVGALLERY_DIR.$data['path'].'thumbs/'))
				mkdir(ADVGALLERY_DIR.$data['path'].'thumbs/', 0777);
			if (!file_exists(ADVGALLERY_DIR.$data['path'].'thumbs/mini/'))
				mkdir(ADVGALLERY_DIR.$data['path'].'thumbs/mini/', 0777);	
											
			$model->insert($data);	
			header('refresh: 3; url=admin,advgallery,categories,pid_'.$this->_pid.'.html');
			$this->view->message = 'Kategoria dodana pomyślnie ! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new AdvGalleryCategories();
		$row = $model->get($this->_id);
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];		
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new AdvGalleryCategoryFilter();
			$filter->filter();
			$data = $filter->getData();
			unset($data['pid']);		
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,advgallery,categories,pid_'.$this->_pid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie ! Przekierowywanie...';	
		}
	}
	/////
	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) {
			$act = clear($_POST['act']);	
			if ($act == 'delete') {
				$model = new AdvGalleryCategories();
				$model->delete($this->_id);
				$model->deleteAllPhotos($this->_id);
				header('refresh: 3; url=admin,advgallery,categories.html');
				$this->view->message = 'Kategoria oraz zdjęcia usunięte pomyślnie ! Przekierowywanie...';
				$this->view->render('categories');		
			} else {
				$cat = clear($_POST['cat']);
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść zdjęcia.'));	
				$model = new AdvGalleryCategories();
				$model->delete($this->_id);
				$model->setCategories($this->_id, $cat);
				header('refresh: 3; url=admin,advgallery,categories.html');
				$this->view->message = 'Kategoria usunięta pomyślnie ! Zdjęcia przeniesione ! Przekierowywanie...';
				$this->view->render('categories');					
			}	
		}
	}	
	
	public function deactiveAction()
	{
		$model = new AdvGalleryCategories();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,advgallery,categories.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie ! Przekierowywanie...';
		$this->view->render('categories');
	}
		
	public function activeAction()
	{
		$model = new AdvGalleryCategories();
		$model->active($this->_id);
		header('refresh: 3; url=admin,advgallery,categories.html');
		$this->view->message = 'Kategoria włączona pomyślnie ! Przekierowywanie...';
		$this->view->render('categories');
	}	
	
	public function settingsAction()
	{
		$segment = $this->_config->segment('advgallery'); 
		
		foreach ($segment as $s)
		{
			$this->view->row[$s['k']] = $s['v'];
		}
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			return;
		}
		if ($this->_request->isPost()) {		
			$pp = (int)clear($_POST['per_page']);
			
			if ($pp == '') $this->_request->redirectFailure(array('Pole "Zdjęć na stronę" jest wymagane.'));
								
			$this->_config->update('per_page', $pp, 'advgallery');

			header('refresh: 3; url=admin,advgallery,settings.html');
			$this->view->message = 'Ustawienia zmienione pomyślnie ! Przekierowywanie...';
		}			
	}
}

?>
