<?php 

class Admin_AdminsController extends Cube_Controller_Abstract
{
	private $_modules;  //spis wszystkich dostepnych modulow bez modules
	private $_modules_access; //spis wszystich modułow bez modules dostępnych dla wybranego id.
	private $_id_admin;	//id admina ktorego chcemy zmienic chaslo.
	
	public function init()
	{		
		$this->view->id = $this->_id = $this->_request->getParam('id'); 
		$model = new Modules();
		$this->_modules=$this->view->rows = $model->getAllModules('name <> "modules" AND active=1', 'name');
		if (IsSet ($this->_id))
		{
			$where='m.name <> "modules" AND m.active=1 AND u.id='.$this->_id;
			$this->_modules_access = $this->view->modules_access = $model->getModules($where, 'm.name');
		}
	}

	public function indexAction()
	{
		$model = new Admins();
		$this->view->rows = $model->getAll();	
	}
	
	private function passwd($id)
	{
		$model = new Admins();
		$id_user = $id;			
		//echo 'Jestem w passwd Id='.$id_user;
		
		if ($this->_request->isRedirected()) {
			//echo 'Jestem w pierwszym IF';
			//exit;
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];	
			return;
		}
		
		if ($this->_request->isPost()) 
		{
			//echo 'Jestem w drugim IFId='.$id_user;
			//return;	
			$filter = new AdminsPasswdFilter();
			$filter->filter();			
			$data = $filter->getData();
			$model = new Admins();
			if ($data['pass'] !== $data['repass']) 
				$this->_request->redirectFailure(array('Podane hasła nie są identyczne. Wielkość liter ma znaczenie.'));			
			
			$model->passwd($data,$id_user);	
			header('refresh: 3; url=admin,admins.html');
			$this->view->message = 'Hasło zmienione pomyślnie ! Przekierowywanie...';
		}	
	}
	
	public function passwdidAction()
	{
		$this->_id_admin=$this->view->id_admin=$this->_id;
		$this->passwd($this->_id_admin);
	}

	public function passwdAction()
	{
		$this->_id = $this->_session->getUserId();			
		$this->passwd($this->_id);
		
		
	}

	public function updateAction()
	{
		//print_r($this->_modules);
		//exit;
		foreach($this->_modules as $row) 
		{
			$id=$row['id'];
			$access = ($_POST['access'.$id]); //nowy wybor usera
			$status=0; //user nie mial uprawnien do modulu
			
			foreach($this->_modules_access as $module_access)
			{
				if($id==$module_access['id'])
				{
					//user mial uprawnienia do modulu
					$status=1; 
					break;
				}
			}
			if($status==0)	
			{
				//echo 'Access='.$access;
				if ($access == '1')
				{
					//jesli zero utaw none
					//echo 'STATUS='.$status.',ID='.$id.'Zmiana accessu. Aktualny access to'.$access;
					$model = new Modules();
					$model->setAccess($this->_id ,$id);
				}
			}
			else
			{
				//jezeli takie same
				if (!(isset($access)))
				{
					//jesli zero utaw $this->_role;
					//echo 'STATUS='.$status.',ID='.$id.'Zmiana accessu. Aktualny access to'.$access;
					$model = new Modules();
					$model->unsetAccess($this->_id ,$id);
				}
			}
		}		
		//admins,edit,id_2.html
		//header('refresh: 3; url=admin,admins.html');
		header('refresh: 3; url=admin,admins,access,id_'.$this->_id.'.html');
		$this->view->message = 'Uprawnienia zmienione pomyślnie ! Przekierowywanie...';
		$this->view->render('add');
		
		
		/*$this->view->rows = $model->getModules('name <> "modules"', 'name');
		$model = new Admins();
		$this->view->role=$model->getRole($this->_id);*/
		//$this->view->message = 'Administrator '.$this->view->role;
	}
	public function accessAction()
	{
		$model = new Admins();
		$login=$model->getLogin($this->_id);
		$this->_login=$this->view->login=$login['login'];
	}	
	
	public function redirectAction()
	{
		header('Location: admin,advgallery.html');
	}
	
	public function editAction()
	{
		$model = new Admins();
		
		$login=$model->getLogin($this->_id);
		$this->view->login=$login['login'];
		$row = $model->get($this->_id);
		$this->view->name 	  = $row['name'];
		$this->view->surname   = $row['surname'];
		$this->view->email 	  = $row['email'];
		$this->view->phone 	  = $row['phone'];
		
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$row= $this->_request->getParamsFromLastRequest(); 
			$this->view->name 	  = $row['name'];
			$this->view->surname   = $row['surname'];
			$this->view->email 	  = $row['email'];
			$this->view->phone 	  = $row['phone'];
			//$this->view->login=$model->getLogin($row['id']);
			//return;
			//header('refresh: 3; url=admin,admins,edit,id_'.$this->_id_admin.'.html');
			//return;
		}
		
		if ($this->_request->isPost()) 
		{		
			$filter = new AdminsEditFilter();
			$filter->filter();			
			$data = $filter->getData();
			$model = new Admins();
			
			if (!preg_match('/^[^@ ]+@[^@ ]+\.[^@ \.]+$/',$data['email']))
				$this->_request->redirectFailure(array('Podano nieprawidłowy adres email.'));	
						
			$id_user=$model->update($data,$this->_id);	
			
			header('refresh: 3; url=admin,admins.html');
			$this->view->message = 'Dane zmienione pomyślnie ! Przekierowywanie...';	
		}
	}
	
	public function addAction()
	{		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];	
			return;
		}
		if ($this->_request->isPost()) {		
			$filter = new AdminsFilter();
			$filter->filter();			
			$data = $filter->getData();
			$model = new Admins();
			
			if ($data['role'] != 'root' && $data['role'] != 'administrator')
				$this->_request->redirectFailure(array('Podano nieprawidłową Rolę admina.'));
			if (!preg_match('/^[^@ ]+@[^@ ]+\.[^@ \.]+$/',$data['email']))
				$this->_request->redirectFailure(array('Podano nieprawidłowy adres email.'));	
			if ($data['pass'] !== $data['repass']) 
				$this->_request->redirectFailure(array('Podane hasła nie są identyczne. Wielkość liter ma znaczenie.'));			
			if ($model->loginExists($data['login']))
				$this->_request->redirectFailure(array('Wybrany login jest już zajęty.'));	
			
			$id_user=$model->insert($data);	
			
			//jezeli zalogowany root  i nowy user to root to nadaj uprawnienia do wszystkich modułów
			if ( ($this->_session->getRole() == 'root') && ($data['role'] == 'root') )
			{
				//pobierz wszystkie aktywne moduły
				$model = new Modules();
				$modules_all=$model->getAllModules('active=1', 'name');
				foreach($modules_all as $row)
				{		
					$id_module=$row['id'];
					$model->setAccess($id_user ,$id_module);
					
				}
			
			}
			header('refresh: 3; url=admin,admins.html');
			$this->view->message = 'Administrator dodany pomyślnie ! Przekierowywanie...';
		}
	}
		
	public function deleteAction()
	{
		if ($this->_id == $this->_session->getUserId()) {
			header('Location: admin,admins.html');
			return;
		}	
		$model = new Admins();
		$model->delete($this->_id);
		
		//usun admina z tabeli admin_access
		$model = new Modules();
		$model->unsetAccess($this->_id ,null);
		
		header('refresh: 3; url=admin,admins.html');
		$this->view->message = 'Administrator usunięty pomyślnie ! Przekierowywanie...';
		$this->view->render('add');
	}

	public function activemailAction()
	{
		if ($this->_id == $this->_session->getUserId()) {
			header('Location: admin,admins.html');
			return;
		}	
		$model = new Admins();
		$model->activeMail($this->_id);
		header('refresh: 3; url=admin,admins.html');
		$this->view->message = 'Status zmieniony pomyślnie ! Przekierowywanie...';
		$this->view->render('add');
	}
	
	public function deactivemailAction()
	{
		if ($this->_id == $this->_session->getUserId()) {
			header('Location: admin,admins.html');
			return;
		}	
		$model = new Admins();
		$model->deactiveMail($this->_id);
		header('refresh: 3; url=admin,admins.html');
		$this->view->message = 'Status zmieniony pomyślnie ! Przekierowywanie...';
		$this->view->render('add');
	}	
	
	public function activeAction()
	{
		if ($this->_id == $this->_session->getUserId()) {
			header('Location: admin,admins.html');
			return;
		}	
		$model = new Admins();
		$model->active($this->_id);
		header('refresh: 3; url=admin,admins.html');
		$this->view->message = 'Status zmieniony pomyślnie ! Przekierowywanie...';
		$this->view->render('add');
	}
	
	public function deactiveAction()
	{
		if ($this->_id == $this->_session->getUserId()) {
			header('Location: admin,admins.html');
			return;
		}	
		$model = new Admins();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,admins.html');
		$this->view->message = 'Status zmieniony pomyślnie ! Przekierowywanie...';
		$this->view->render('add');
	}	
	
	public function roleAction()
	{
		if ($this->_id == $this->_session->getUserId()) {
			header('Location: admin,admins.html');
			return;
		}	
		$model = new Admins();
		$role = clear($_POST['role']);
		if ($role == 'root' && $this->_session->getRole() == 'administrator') header('Location: admin,admins.html');
		if ($role == 'root' || $role == 'administrator')
			$model->setRole($this->_id, $role);
		header('refresh: 3; url=admin,admins.html');
		$this->view->message = 'Admin zaktualizowany pomyślnie ! Przekierowywanie...';
		$this->view->render('add');		
	} 			
}

?>
