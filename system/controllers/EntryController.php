<?php

class EntryController extends Cube_Controller_Abstract
{
	private $_pp;
	private $_where_cat;	//where dla kategorii
	private $_where_doc;	//where dla dokument�w
	public function init()
	{

		$this->view->username = $this->_session->getUsername();
		$this->_pp = $this->_config->get('per_page', 'entry');

		$this->_where_cat=NULL;
		$this->where_doc=NULL;
	}


	private function show_index($cid)
	{
		$this->view->cid = $cid;

		$model = new EntryTree();
		$this->view->info = $model->get($cid);
		$this->view->left_menu = $model->leftMenuTree($cid,'pos','name');
		//echo 'INFO <pre>';
		//	print_r($this->view->left_menu);
		//echo '</pre>';

		$this->view->left_menu=$this->show_tree($this->view->left_menu);

		$model = new EntryDocs();
//		var_dump($cid);
//		var_dump();
//		var_dump($this->_where_doc.'cid = '.$cid);
		$allIds = EntryCategories::getAllIds($cid);
		if($allIds) {
			$allIds = array_unique($allIds);
		}
		if($allIds && count($allIds)>1) {
			$this->view->docs = $model->getAll($this->_where_doc.'cid IN ('.  implode(',', $allIds).')',title);
		}
		else {
			$this->view->docs = $model->getAll($this->_where_doc.'cid = '.  $cid,title);
		}


		/*$this->view->klips = $model->getMovies();
		$this->view->photos = $model->getPhotos();
		$this->view->docs = $model->getDocs();*/
	}

	private function create_where_doc($search)
	{
		$where_id='d.id like "%'.$search.'%"';
		$where_title='d.title like "%'.$search.'%"';
		$where_desc='d.description like "%'.$search.'%"';
		//$where_filename='d.filename like "%'.$search.'%"';
		$where='(' .$where_id.' OR '.$where_title .' OR '.$where_desc.')';
		return $where;

	}

	private function create_where_cat($search)
	{
		$where_name='name like "%'.$search.'%"';
		$where='('.$where_name.')';
		return $where;

	}


	public function searchAction()
	{
		$search_doc=$_POST['search_doc'];
		$search_cat=$_POST['search_cat'];
		$this->view->search_doc = $this->_search_doc = $search_doc;
		$this->view->search_cat = $this->_search_cat = $search_cat;
		$this->_where_doc=$this->create_where_doc($search_doc);
		$this->_where_cat=$this->create_where_cat($search_cat);
		$this->view->render('index');
		$this->indexAction();

	}



	public function indexAction()
	{
		{
			$cid = $this->_request->getParam('cid', 1);

			if ($cid != 1)
			{
				if (isSet($this->_where_doc))
					$this->_where_doc.=' AND ';
				$this->show_index($cid);
				$this->view->all=0;
			}
			else
			{
				$this->view->all=1;
				$this->view->cid = $cid;

				$model = new EntryTree();
				$info = $this->view->info = $model->getAllTree($this->_where_cat,'t.pos','t.name');
				$this->view->left_menu = $model->leftMenuTree($cid,'pos','name');

				/*echo 'INFO <pre>';
							print_r($this->view->info);
							echo '</pre>';*/

				$this->view->left_menu=$this->show_tree($this->view->left_menu);
				$this->view->info=$this->show_tree($this->view->info);
				$model = new EntryDocs();
				//$this->view->klips = $model->getMovies();
				//$this->view->photos = $model->getPhotos();
//				var_dump($this->_where_doc);
				$this->view->docs = $model->getAll($this->_where_doc,title);


			}
		}
	}

	//------------------------------------------------------------------------------------------
	//TREE

	private function get_children($id,$parent_id,$rows,&$tree,&$depth)
	{
		//funkcja tworzy drzewo do wyswietlania (zmiana kolejnosci wierszy).
		//$rows	- tablica zawierajaca wiersze drzewa posortowane w kolejnosci poziom,(id lub name lub pos).
		//$parent_id - tablica zawierajaca informacje o rodzich i ich dzieciach

		//funcja zwraca
		//$tree - struktura drzewa do wyswietlania
		//pojedynczy lisc drzewa. Dzieki rekurencji z pojedynczych drzew tworzy sie nowe drzewo.

		//echo 'ID='.$id;
		//znajdz index elementu (lisc) o zadanym id

		for ($i=0;$i < count($rows);$i++)
			if($rows[$i]['id']==$id)
			{
				$index=$i;
				break;
			}
		//echo 'index='.$index;
		//skopiuj nowy lisc do drzewa
		//static $depth2=0;
		//$depth2+=1;
		$rows[$index]['depth']=$depth;
		$leaf=$rows[$index];
		$tree[]=$leaf;

		/*(echo'TREE';
		echo '<pre>';
		print_r($tree);
		echo '</pre>';*/

		//echo 'ID='.$id;
		if(isset($parent_id[$id]))
		{

			$depth+=1;
			//echo 'DEPTH='.$depth;
			//echo 'JESTEM w IF';
			//jezeli dziecko ma dzieci
			$children=$parent_id[$id];
			//print_r($children);
			foreach ($children as $child)
			{
				//echo 'CHILD='.$child;
				//dla kazdego dziecka pobierz jego dzieci
				$this->get_children($child,$parent_id,$rows,$tree,$depth);
				//echo'TREE2';
				//echo '<pre>';
				//print_r($tree);
				//echo '</pre>';
			}
			$depth-=1;
		}
		//else
		//{
		//	return $leaf;
		//}
	}

	private function show_tree($rows)
	{

		$parent_id=array();

		for ($i=0;$i < count($rows);$i++)
			for ($j=$i+1;$j<count($rows);$j++)
				if ($rows[$i]['id']==$rows[$j]['parent_id'])
					$parent_id[$rows[$i]['id']][]=$rows[$j]['id'];


		//echo '<pre>';
		//echo print_r($parent_id);
		//echo '</pre>';

		$tree=array();
		for ($i=0;$i < count($rows);$i++)
		{
			//jezeli wiecej niz jeden korzen (parent_id=1)
			if ($rows[$i]['parent_id']==1)
			{
				$id=$rows[$i]['id'];
				$depth=0;
				$this->get_children($id,$parent_id,$rows,$tree,$depth);
				$tree+=$tree;
			}
		}
		/*echo '<pre>';
		echo print_r($tree);
		echo '</pre>';

		foreach ($tree as $leaf)
		{
			echo $leaf['id'].'-'.$leaf['name'].'-'.$leaf['depth'].'<br>' ;
		}*/
		return $tree;
	}


}

?>
