<?php

require_once 'Cube/Filter/Abstract.php';

class EntryCategoryFilter extends Cube_Filter_Abstract
{
	public function filter()
	{	
		$filters    = array('name' => 'Clear', 'cid' => 'Clear', 'description' => 'Clear');
		$validators = array('name' => 'Required', 'cid' => 'Required');
		$messages   = array('name' => 'Pole "Tytuł" jest wymagane', 'cid'=> 'Pole "Kategoria" jest wymagane'); 	
		$this->_process($filters, $validators, $messages);	
	}
}
					 
?>
