<?php

require_once 'Cube/Filter/Abstract.php';

class AdminsPasswdFilter extends Cube_Filter_Abstract
{
	public function filter()
	{	
		$filters    = array('pass' => 'Clear',
							'repass' => 'Clear');
		$validators = array('pass' => 'Required', 'repass' => 'Required');
		$messages   = array('login' => 'Pole "Login" jest wymagane', 'pass' => 'Pole "Hasło" jest wymagane', 'repass' => 'Pole "Powtórz Hasło" jest wymagane'); 
		$this->_process($filters, $validators, $messages);	
	}
}
					 
?>
