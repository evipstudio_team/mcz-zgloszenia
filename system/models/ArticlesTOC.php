<?php

require_once 'Cube/Model/Abstract.php';

class ArticlesTOC extends Cube_Model_Abstract
{
	protected $_name = 'articles-toc';
	
		public function getAll($where = null, $order = null)
	{
		if (!is_null($where)) $where = ' WHERE '.$where;
		if (!is_null($order)) $order = ' ORDER BY '.$order;	
		$r = mysql_query('SELECT * FROM `articles-toc`'.$where.$order);
		return mysql_fetch_all($r);
	}	
	
	public function get($id)
	{
		$r = mysql_query('SELECT * FROM `articles-toc` WHERE id = "'.$id.'"');
		return mysql_fetch_assoc($r);
	}	
	
	public function insert($d)
	{
		mysql_query('INSERT INTO `articles-toc` VALUES (null, "'.$d['name'].'", "'.$d['contents'].'", "'.$d['language'].'", "'.$d['logged'].'")');
	}
	
	public function update($id, $d)
	{
		mysql_query('UPDATE `articles-toc` SET name = "'.$d['name'].'", contents = "'.$d['contents'].'", language = "'.$d['language'].'", logged = "'.$d['logged'].'" WHERE id = "'.$id.'"');
	}
	
	public function delete($id)
	{
		mysql_query('DELETE FROM `articles-toc` WHERE id = "'.$id.'"');
	}			
}
					 
?>
