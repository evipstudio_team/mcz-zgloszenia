<?php
define('ENTRY_DIR_BIG_PHOTOS', 'public/entry/');
define('ENTRY_DIR_THUMBS', 'public/entry/thumbs/');
define('ENTRY_DIR_MINI', 'public/entry/mini/');
define('ENTRY_DIR_TMP', 'public/entry/tmp/');
define('ENTRY_THUMB_WIDTH', 330);
define('ENTRY_THUMB_HEIGHT', 215);
define('ENTRY_MWIDTH', 50);
define('ENTRY_MHEIGHT', 35);
define('ENTRY_DIR', 'public/entry/');

require_once 'Cube/Model/Abstract.php';

class Entry extends Cube_Model_Abstract
{
	protected $_name = 'entry';
	
	public function insert($data)
	{
		$data['add_date'] = time();
		parent::insert($data);
		$this->inc($data['cid']);		
	}
	
 	public function insertPhoto($data)
	{
  		$data['add_date'] = time();
		$sql = 'INSERT INTO entry_photos VALUES (null, "'.$data['cid'].'", 
					 "'.$data['add_date'].'", "'.$data['filename'].'", "'.$data['title'].'", "'.$data['description'].'")';
	
	   mysql_query($sql);
  }
    
	public function insertDocs($data)
	{
  		$data['add_date'] = time();
		$sql = 'INSERT INTO entry_docs VALUES (null, "'.$data['cid'].'", 
					 "'.$data['add_date'].'", "'.$data['filename'].'", "'.$data['title'].'", "'.$data['description'].'", "'.$data['size'].'")';

	   mysql_query($sql);
	}	
	
	public function delete($id)
	{
		$row = $this->get($id);
 		if($row['filename'] != null)
			if (file_exists('public/entry/movie/'.$row['filename'])) unlink('public/movie/movie/'.$row['filename']);
   	//mysql_query('DELETE FROM entry WHERE id = "'.$id.'"');
		$this->dec($row['cid']);	
		parent::delete($id);
	}

	public function update($id, $data)
	{
		$data['oldCid'] = clear($_POST['oldCid']);
		if ($data['cid'] != $data['oldCid'])
		{	
			$this->dec($data['oldCid']);
			$this->inc($data['cid']);
		}
		unset($data['oldCid']);	// usuwa wpis zeby uniknac kolizji w update
		parent::update($id, $data);
	}

	public function updatePhoto($id, $data)
	{
		mysql_query('UPDATE entry_photos SET title = "'.$data['title'].'", description = "'.$data['description'].'", cid = "'.$data['cid'].'"
							WHERE id = "'.$id.'"');
	}
	
	public function updateDoc($id, $data)
	{
		mysql_query('UPDATE entry_docs SET title = "'.$data['title'].'", description = "'.$data['description'].'", cid = "'.$data['cid'].'"
							WHERE id = "'.$id.'"');
	}
	
	public function get($id)
	{
		$r = mysql_query('SELECT p.*, c.path, c.name FROM entry p, entry_categories c
		WHERE p.id = "'.$id.'" AND p.cid = c.id');
		if($r != null)
			return mysql_fetch_assoc($r);
		else 
			return null;
	}
	
	public function getAll($where = null, $order = null)
	{
		if (!is_null($where)) $where = ' AND '.$where;
		if (!is_null($order)) $order = ' ORDER BY '.$order;
		$sql = 'SELECT p.*, c.path, c.name, c.id as cat FROM entry p, entry_categories c
		WHERE p.cid = c.id'.$where.$order;
	
		$r = mysql_query($sql);
		if($r != null)
			return mysql_fetch_all($r);	
		else 
			return null;
	}
	
	public function getCats()
	{
   	$sql = 'SELECT * FROM entry_categories';
 		$r = mysql_query($sql);
 		return mysql_fetch_all($r);
 	}
	
	public function getDocs($where = null,$order = null)
	{
	if (!is_null($where)) $where = 'WHERE '.$where;
	if (!is_null($order)) $order = ' ORDER BY '.$order;
	$sql = 'SELECT * FROM entry_docs '.$where.$order;
	//echo $sql;	
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_all($res);
  		}else {
    	return null;
  		}
 	}	
	
	public function getPhotos($where = null)
	{
		$sql = 'SELECT * FROM entry_photos  '.$where;
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_all($res);
  		}else {
    	return null;
  		}
 	}
 	
 	public function getMovies($where = null)
	{
		$sql = 'SELECT * FROM entry  '.$where;
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_all($res);
  		}else {
    	return null;
  		}
 	}
	
	private function inc($cid)
	{
		mysql_query('UPDATE entry_categories SET p_amount = p_amount+1 WHERE id = "'.$cid.'"');
	}
	
	private function dec($cid)
	{
		mysql_query('UPDATE entry_categories SET p_amount = p_amount-1 WHERE id = "'.$cid.'"');
	}
	
	public function deleteAllPhotos($id)
	{
   	$sql = 'SELECT * FROM entry_photos WHERE cid = "'.$id.'"';
   	$res = mysql_query($sql);
   	$res = mysql_fetch_all($res);
   	
   	foreach($res as $r){
    		$this->deletePhoto($r['id']);
 		}
   
 	}
 	
 	public function deleteAllDosc($id)
 	{
   	$sql = 'SELECT * FROM entry_docs WHERE cid = "'.$id.'"';
   	$res = mysql_query($sql);
   	$res = mysql_fetch_all($res);
   	
   	foreach($res as $r){
    		$this->deleteDocs($r['id']);
 		}    
  	}
	
	public function deletePhoto($id){
   	$p = $this->getPhotos(' where id = '.$id);
   	if($p['filename'] != null){
			if (file_exists(ENTRY_DIR_BIG_PHOTOS.$p['filename'])) unlink(ENTRY_DIR_BIG_PHOTOS.$p['filename']);
			if (file_exists(ENTRY_DIR_THUMBS.$p['filename'])) unlink(ENTRY_DIR_THUMBS.$p['filename']);
			if (file_exists(ENTRY_DIR_MINI.$p['filename'])) unlink(ENTRY_DIR_MINI.$p['filename']);
		}
   	mysql_query('DELETE FROM entry_photos WHERE id = "'.$id.'"');
 	}
 	
 	public function deleteDocs($id) {
    	$p = $this->getDocs(' where id = '.$id);
    	if($p['filename'] != null)
			if (file_exists('public/entry/docs/'.$p['filename'])) unlink('public/entry/docs/'.$p['filename']);
			
		$sql = 'DELETE FROM entry_docs WHERE id = "'.$id.'"';
		//echo $sql;
		//exit;	
   	mysql_query($sql);   
  	}
		
}
					 
?>
