<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			$files = '<select class="short" name="exists_filename">';
		if ($this->row['exists_filename'] == '') $files .= '<option value="" selected="selected"> </option>';	
		foreach((array)$this->files as $file)
		{
			if ($this->row['exists_filename'] == $file) $files .= '<option value="'.$file.'" selected="selected">'.$file.'</option>';
			else $files .= '<option value="'.$file.'">'.$file.'</option>';	
		}	
		$files .= '</select>';
	
			echo '<form action="admin,advnews,edit,id_'.$this->row['id'].'.html" method="post">
				<fieldset>
					<legend>Edytuj newsa</legend>
					<div><label for="language"><span class="b">Język:</span></label><select class="short" name="language">'.getSelectLanguages($this->row['language']).'</select></div>
					<div><label for="author"><span class="b">Autor:</span></label><input type="text" name="author" class="short" value="'.$this->row['author'].'" /></div>			
					<div><label for="title"><span class="b">Tytuł:</span></label><input type="text" name="title" value="'.$this->row['title'].'" /></div>
					<div><label for="exists_filename"><span>Obrazek:</span></label>'.$files.'</div>
					<div><label for="meta_title"><span>Meta title:</span></label><textarea class="short" name="meta_title" rows="4">'.$this->row['meta_title'].'</textarea></div>
					<div><label for="contents"><span class="b">Treść:</span></label></div>
					<div><textarea class="tiny" name="contents" rows="28">'.$this->row['contents'].'</textarea></div>
					<!--<div><label for="short_contents"><span>Streszczenie:</span></label></div>
					<div><textarea class="tiny" name="short_contents" rows="4">'.$this->row['short_contents'].'</textarea></div>-->
				<div><p><b>KATEGORIE:</b></p></div>
				';
				
				$catContent = '';
				foreach ((array)$this->cats as $c)
				{
					if ($c['id'] == 1) {
						if (isset($this->catsList[$c['id']])) {
							echo '<div><label for="kat"><span>Główna:</span></label> <input type="checkbox" name="cat_value[]" value="1_'.$c['id'].'" checked="checked" /> <input type="hidden" class="hidden" name="cat_id[]" value="1" /></div>';
						}
						else echo '<div><label for="kat"><span>Główna:</span></label> <input type="checkbox" name="cat_value[]" value="1_'.$c['id'].'" /> <input type="hidden" class="hidden" name="cat_id[]" value="1" /></div>';
					} else {
						if (isset($this->catsList[$c['id']])) {
							$catContents .=  '<div><label for="kat"><span>'.$c['name'].':</span></label> <input type="checkbox" name="cat_value[]" value="1_'.$c['id'].'" checked="checked" /> <input type="hidden" class="hidden" name="cat_id[]" value="'.$c['id'].'" /></div>';
						}
						else $catContents .=  '<div><label for="kat"><span>'.$c['name'].':</span></label> <input type="checkbox" name="cat_value[]" value="1_'.$c['id'].'" /> <input type="hidden" class="hidden" name="cat_id[]" value="'.$c['id'].'" /></div>';
					}
				
				}
					
					echo $catContents.'
					<div><p>* Pole "Streszczenie" to treść która zostanie wyświetlona w bloku "Najnowsze aktualności"<br />
						* Gdy pole "Streszczenie" zostanie puste, w bloku zostanie wyświetlone pierwsze 200 znaków treści.<br />
						* Pola <strong>pogrubione</strong> są wymagane.</p></div>
					<div>
						 <input type="hidden" class="hidden" name="id" value="'.$this->row['id'].'" />
						 <input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first" />			
						 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
			</form>';
	}
?>
