<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			echo '<form action="admin,advnews,editcomment,id_'.$this->row['id'].'.html" method="post">
				<fieldset>
					<legend>Edytuj komentarz</legend>
					<div><label for="author"><span class="b">Autor:</span></label><input type="text" name="author" class="short" value="'.$this->row['author'].'" /></div>			
					<div><label for="contents"><span class="b">Treść:</span></label><textarea name="contents" rows="8">'.$this->row['contents'].'</textarea></div>
					<div><p>* Pola <strong>pogrubione</strong> są wymagane.</p></div>
					<div>
						 <input type="hidden" class="hidden" name="id" value="'.$this->row['id'].'" />
						 <input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first" />			
						 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
			</form>';
	}
?>
