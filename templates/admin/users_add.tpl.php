<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {	
		//print_r($this->row);
		//exit;
		//if ($this->_session->getRole() == 'root')
		//	$allRole = '<option value="root">root</option><option value="administrator" selected="selected">administrator</option>';
		//else
		//	$allRole = '<option value="user" selected="selected">user</option>';
		
		//oryginal
		/*switch ($this->row['role'])
		{
			case 'root': $allRole = '<option value="root" selected="selected">root</option><option value="administrator">administrator</option>'; break;
			default: $allRole = '<option value="root">root</option><option value="administrator" selected="selected">administrator</option>';
		}*/
	
		echo '
		<form id="add-admin" action="admin,users,add.html" method="post">
			<fieldset>
				<legend>Dodaj użytkownika</legend>
				<div><label for="login"> <span><strong>Login:</strong></span></label>  <input class="short" name="login" id="login" type="text" value="'.$this->row['login'].'" /></div>
				<div><label for="name"> <span><strong>Imię:</strong></span></label>  <input class="short" name="name" id="name" type="text" value="'.$this->row['name'].'" /></div>
				<div><label for="surname"> <span><strong>Nazwisko:</strong></span></label>  <input class="short" name="surname" id="surname" type="text" value="'.$this->row['surname'].'" /></div>
				<div><label for="pass"> <span><strong>Hasło:</strong></span> </label>  <input class="short" name="pass" type="password" value="'.$this->row['pass'].'" /></div>
				<div><label for="repass"> <span><strong>Powtórz hasło:</strong></span> </label>  <input class="short" name="repass" type="password" value="'.$this->row['repass'].'" /></div>
				<div><label for="email"> <span><strong>Email:</strong></span> </label>  <input class="short" name="email" id="email" type="text" value="'.$this->row['email'].'" /></div>	
				<div><label for="phone"> <span>Telefon:</span> </label>  <input class="short" name="phone" id="phone" type="text" value="'.$this->row['phone'].'" /></div>	
				<div><p>* Pola <strong>pogrubione</strong> są wymagane.</p></div>
				<div>
					<input type="submit" class="submit-first" value="dodaj" />
					<input type="reset" class="submit" value="anuluj" />
				</div>
				</fieldset>									
		</form>';
	}

?>
