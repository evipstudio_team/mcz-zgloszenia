<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		echo '
		<form id="add_cat" action="admin,advnews,addcat.html" method="post">
			<fieldset>
				<legend>Dodaj kategorię</legend>';
		
				
		echo '
		<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" id="name" value="" /></div>
		<div><label for="max_chars_main"><span class="">Max. znaków - strona główna:</span></label><input type="text" name="max_chars_main" id="max_chars_main" value="" /></div>
		<div><label for="max_chars_advnews"><span class="">Max. znaków - aktualności:</span></label><input type="text" name="max_chars_advnews" id="max_chars_advnews" value="" /></div>	
		<div><p>Pola pogrubione są wymagane.</p></div>
				<div>
					 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first input" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit input" />
				</div></fieldset>
		</form><br /><br />';

			echo '<table id="administrate" class="clear">
					<thead>
						<tr>
							<td>ID</td>
							<td>Nazwa</td>
							<td>Max znakow - główna</td>
							<td>Max znakow - aktualności</td>
							<td>Ilość newsów</td>
							<td>Status</td>	
							<td>Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			
			if (count($rows) < 1) {
				echo '<tr><td colspan="6">Nie odnaleziono żadnych kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					
					if ($r['active']) {
						$status = '<span class="green">aktywna</span>';
						$status_action = ' | <a href="admin,advnews,deactive,id_'.$r['id'].'.html">Deaktywuj</a>';
					} else {
						$status = '<span class="red">nieaktywna</span>';
						$status_action = ' | <a href="admin,advnews,active,id_'.$r['id'].'.html">Aktywuj</a>';				
					} 				
					
					$del = '<br><a href="admin,advnews,deletecat,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a>';
					$edit = '<a href="admin,advnews,editcat,id_'.$r['id'].'.html">Edytuj</a> ';
					if ($r['id'] == 1) {
						$del = null;
						$status_action = null;
						$edit = 'brak';
					} 
					 				
					echo '			<tr'.$class.'>
							<td>'.$r['id'].$del.'</td>
							<td>'.$r['name'].'</td>
							<td>'.$r['max_chars_main'].'</td>
							<td>'.$r['max_chars_advnews'].'</td>
							<td>'.$r['amount'].'</td>
							<td>'.$status.'</td>
							<td>'.$edit.' '.$status_action.'</td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>';
	}

?>
