<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			$comments = null;
			if ($this->row['comments'] == '1') $comments = ' checked="checked"';
			
			echo '<form action="admin,advnews,settings.html" method="post">
				<fieldset>
					<legend>Konfiguracja</legend>
					<div><label for="per_page" class="long"><span class="b">Newsów na stronę:</span></label><input type="text" name="per_page" class="short" value="'.$this->row['per_page'].'" /></div>			
					<div><label for="max_chars" class="long"><span class="b">Max znaków komentarza:</span></label><input type="text" class="short" name="max_chars" value="'.$this->row['max_chars'].'" /></div>
					<div><label for="comments" class="long"><span>Komentarze:</span></label><input type="checkbox" class="check" name="comments" value="1"'.$comments.' /></div>
					<div>	 
						 <input type="submit" name="submit" id="submit" value="zapisz" class="submit-first-long" />			
						 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
			</form>';
	}
?>
