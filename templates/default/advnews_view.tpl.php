<?php

	/* NIE RUSZAC */
	$row = $this->news;			/* row = tablica zawierająca wszystkie info odnośnie newsa:
								 $row['id'] = id newsa
								 $row['add_date'] = data dodania, timestamp (patrz opis add_date w subpage.tpl.php)
								 $row['author'] = autor newsa
								 $row['title'] = tytuł newsa
								 $row['meta_title'] = meta title newsa
								 $row['contents'] = pełna treśc newsa
								 $row['short_contents'] = krótka treśc newsa podana w panelu
								 $row['comments_amount'] = ilośc komentarzy
								*/
								
	$comments = $this->comments; /* comments = tablica dwuwymiarowa zawierająca wszystkie komenatrze do newsa
									   należy ją potraktować pętlą foreach, przyklad poniezej.
									*/						
	$statusComments = $this->statusComments; //	komentarz włączone (true) lub nie (false)
	$id = $this->id;													
	/* END NIE RUSZAC */

	echo 'Meta title = '.$row['meta_title'];
	echo '<h1>Aktualności</h1>';
	
	if (count($row) < 1) echo '<p>Nie odnaleziono newsa o podanym ID w bazie.</p>';
	else {

		$t_img = '';
		if ($row['filename'] != '') $t_img = '<img src="public/news/thumbs/' . $row['filename'] . '" alt="" />';
		
		echo '<div class="akt2">
			 <h2>'.$row['title'].'</h2>
			 '.$t_img.''.$row['contents'].'<br clear="all" />
			 <a href="javascript:history.back();" class="more">Powrót</a></div>';
	}

	
?>

