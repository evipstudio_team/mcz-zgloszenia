<?php

	/* NIE RUSZAC */
	$news = $this->rows;			/* news = tablica dwuwymiarowa zawierająca wszystkie newsy wyświetlane na danej podstronie
									   należy ją potraktować pętlą foreach, przyklad poniezej.
									*/
	$pages = $this->pages;			// obiekt operujący na stronnicowaniu wyników, opis poniżej z przykladem	
	$statusComments = $this->statusComments; //	komentarz włączone (true) lub nie (false)											
	/* END NIE RUSZAC */
$latestNewsBlock = $this->getBlock('latestNews');	
	$latestNews = (array)$latestNewsBlock->getLatest();	
	
	// wyświetla wszystkie newsy:
	
	
		echo '<div class="menu_l">
	<div>Nowości</div>';
	
	foreach ($latestNews as $n)
	{	
 		echo '<a href="aktualnosci_pokaz,'.$n['id'].'.html">'.$n['title'].'</a>';
	}
	
	echo '<a href="aktualnosci_wszystkie.html">wszystkie...</a></div><div class="rightside"><div class="nag">Nowości</div>';
	 
	if (count($news) < 1) echo 'Nie odnaleziono żadnych newsów w bazie.';
	else {
	
		/* $n dostępne w foreach to tablica zawierająca jednego newsa, dane:
		 $n['id'] = id newsa
		 $n['add_date'] = data dodania, timestamp (patrz opis add_date w subpage.tpl.php)
		 $n['author'] = autor newsa
		 $n['title'] = tytuł newsa
		 $n['contents'] = pełna treśc newsa
		 $n['short_contents'] = krótka treśc newsa podana w panelu
		 $n['comments_amount'] = ilośc komentarzy
		*/ 
		foreach ($news as $n) 
		{
			echo '<p>'.date('d.m.Y H:i', $n['add_date']).' - <a href="aktualnosci_pokaz,'.$n['id'].'.html">'.$n['title'].'</a></p>';
		}
	}
	echo '</div>';
	
	/* a teraz najtrudniejsze... opis tego znajduje się ponizej, zeby bylo czytelne	
	if ($pages->hasPrev() || $pages->hasNext()) { 
		echo '<div class="pages-nav">';
		
		if ($pages->hasPrev()) {
			echo $pages->getPrevLink('<a href="aktualnosci,:value.html" class="prev">Poprzednia</a>');
		} else {
			echo '<a href="#" class="prev">Poprzednia</a>';
		}
		
		echo '<p>Strony: '.$pages->getPages() .'</p>';
		
		if ($pages->hasNext()) {
			echo $pages->getNextLink('<a href="aktualnosci,:value.html" class="next">Następna</a>');
		} else {
			echo '<a href="#" class="next">Następna</a>';
		}
		
		echo '</div>';
	}	*/	

	/*
		OPIS STRONNICOWANIA (jest to przykład tego, jak ja to stosuję):
		1. warunek if ($pages->hasPrev() || $pages->hasNext()) { } to warunek, który sprawdza czy należy wyświetlić listę
		   możliwych stron; wyświetla je tylko jeżli strona posiada następna lub poprzednią strone 
		
		2. warunek if ($pages->hasPrev()) {} sprawdza czy istnieje poprzednia strona, jeśli tak to wyświetla to,
		   co jest w szablonie strony, czyli: $pages->getPrevLink(SZABLON);
		   musimy to tak wyświetlać poniewaz nie wiemy jaki numer ma poprzednia strona, generuje to dynamicznie,
		   tak prawdę mówiąc to dalem to dlatego żebyś mogł edytować sobie classy tych odnośników poprzednich,
		   czyli Ciebie interesuje tylko to: class="prev">Poprzednia</a>, reszta musi zostać jak jest.
		   
		   Jeśli strona nie istnieje wyswietla: <a href="#" class="prev">Poprzednia</a> lub cokolwiek innego co zechcesz
		   	   	
		3. echo '<p>Strony: '.$pages->getPages() .'</p>';
			ta linia wyświetla wszystkie dostepne strony w postaci [1] [2] [3] etc... 
		
		4. warunek if ($pages->hasNext()) {} sprawdza czy istnieje następna strona, jeśli tak to wyświetla to,
		   co jest w szablonie strony, czyli: $pages->getNextLink(SZABLON);
		   musimy to tak wyświetlać poniewaz nie wiemy jaki numer ma nastepna strona, generuje to dynamicznie,
		   tak prawdę mówiąc to dalem to dlatego żebyś mogł edytować sobie classy tych odnośników nastepnych,
		   czyli Ciebie interesuje tylko to: class="next">Następna</a>, reszta musi zostać jak jest.
		   
		   Jeśli strona nie istnieje wyswietla: <a href="#" class="next">Następna</a> lub cokolwiek innego co zechcesz
		
		
		wynik działania tego przykładu możesz zobaczyc tutaj:  http://nexter.pl/aktualnosci.html
		gdy podstron jest bardzo duzo to wyswietaja sie tak:
		[1] [2] [3] [4] [5] [6] ... [15] [16] [17] ... [28] [29] [30]
		czyli pierwsze, srodkowe i ostatnie, oczywiscie to sie zmienia, potestujesz sobie.
   	   				
	*/

?>
